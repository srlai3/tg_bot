from telegram.ext import Updater, InlineQueryHandler, CommandHandler
import requests
import re

def get_url():
    contents = requests.get('https://random.dog/woof.json').json()    
    #contents = requests.get('https://bitbucket.org/srlai3/tg_bot/src/master/Images/photo_2019-10-11_13-53-12.jpg').jpg()
    url = contents['url']
    return url

def get_txt():
	import urllib.request
	url = 'https://bitbucket.org/srlai3/tg_bot/raw/05853aade61defb2e8781a25ba8398ac93a64075/src/src.txt'
	response = urllib.request.urlopen(url)
	data = response.read()      # a `bytes` object
	texts = data.decode('utf-8') # a `str`; this step can't be used if data is binary
	text= texts.split()
	print(text)
	return text
	# f=open("/home/roy/Desktop/Workspace/tg_bot/src/src.txt","r")
	# contents=f.read()
	# links=contents.split()
	# print(links)
	# return links

def bop(bot, update):
    url = get_url()
    links=get_txt()
    chat_id = update.message.chat_id
    for link in links:
    	bot.send_photo(chat_id=chat_id, photo=link)

def boe(bot, update):
    #context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")
    url = get_url()
    chat_id = update.message.chat_id
    update.message.reply_text("Hello")


def main():
    updater = Updater('859575730:AAFykNqrKQpHdlCD5xUoP3zk7HfxfKTYucc')
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('bop',bop))
    dp.add_handler(CommandHandler('boe',boe))
    get_txt()
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()
